﻿using System.Collections.Generic;
using ScriptableObjectClasses;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;
using static Utilities.RenderScreenUtilities;
using POCOs;

public class SimulatedParticleRenderer : MonoBehaviour
{
    public int Width => dimensions.value.x;
    public int Height => dimensions.value.y;
    
    [SerializeField] private Material renderTextureMaterial;
    [SerializeField] private Vector2IntObject dimensions;
    [SerializeField] private SimulatorParticlesObject particlesToRender;
    
    private RenderTexture _targetRenderTexture;
    private Dictionary<ParticleType, Color> _colorMap;

    private void Awake()
    {
        _colorMap = new Dictionary<ParticleType, Color>
        {
            { ParticleType.Empty, Color.white },
            { ParticleType.Sand, new Color(0.85f, 0.82f, 0.53f) },
            { ParticleType.Water, new Color(0.14f, 0.48f, 0.96f) }
        };
        
        _targetRenderTexture = new RenderTexture(Width, Height, depth: 0)
        {
            dimension = TextureDimension.Tex2D,
            antiAliasing = 1,
            graphicsFormat = GraphicsFormat.R8G8B8_UNorm,
            useMipMap = false,
            filterMode = FilterMode.Point,
            anisoLevel = 0
        };
        
        int mainTex = Shader.PropertyToID("_MainTex");
        renderTextureMaterial.SetTexture(mainTex, _targetRenderTexture, RenderTextureSubElement.Color);
    }

    private void Update()
    {
        if (particlesToRender.updateQueued)
        {
            Render(particlesToRender.particles);
            particlesToRender.updateQueued = false;
        }
    }

    private void Render(Particle[,] particles)
    {
        var texture = new Texture2D(Width, Height);
        var colors = CreateColorArray(particles);

        texture.SetPixels(colors);
        texture.Apply();

        Graphics.Blit(texture, _targetRenderTexture);
    }

    private Color[] CreateColorArray(Particle[,] particles)
    {
        var colors = new Color[Width * Height];
        
        for (int y = 0; y < Height; y++)
        {
            for (int x = 0; x < Width; x++)
            {
                var particle = particles[x, y];
                var particleType = particle.particleType;
                var color = _colorMap[particleType];
                var index = CalculateIndex(x, y, Width);

                colors[index] = color;
            }
        }
        
        return colors;
    }
}
