﻿using System.Collections.Generic;
using POCOs.ParticleAlgorithms;

namespace POCOs
{
    public class ParticleSimulator
    {
        public Particle[,] Particles { get; }
        public int Width { get; }
        public int Height { get; }

        private readonly Dictionary<ParticleType, BaseParticleAlgorithm> _simulationAlgorithms =
            new Dictionary<ParticleType, BaseParticleAlgorithm>()
        {
            { ParticleType.Sand, new SandAlgorithm() }
        };

        public ParticleSimulator(int width, int height)
        {
            Particles = new Particle[width, height];
            Width = width;
            Height = height;
        }

        public void UpdateParticles()
        {
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    var particle = Particles[x, y];
                    var particleType = particle.particleType;

                    if (particleType == ParticleType.Empty)
                        continue;
                    
                    var algorithm = _simulationAlgorithms[particleType];
                    algorithm.Execute(x, y, Particles);
                }
            }
        }

        /// <summary>
        /// Create a particle at the designated coordinates, but only if the cell is empty.
        /// </summary>
        /// <param name="x">The X coordinate.</param>
        /// <param name="y">The Y coordinate.</param>
        /// <param name="particle">The particle to create.</param>
        public void SetParticleIfFree(int x, int y, Particle particle)
        {
            var currentParticle = Particles[x, y];
            if (currentParticle.particleType == ParticleType.Empty)
                Particles[x, y] = particle;
        }
    }
}