﻿namespace POCOs
{
    public readonly struct Particle
    {
        public readonly ParticleType particleType;

        public Particle(ParticleType particleType)
        {
            this.particleType = particleType;
        }
    }

    public enum ParticleType
    {
        Empty,
        Sand,
        Water
    }
}