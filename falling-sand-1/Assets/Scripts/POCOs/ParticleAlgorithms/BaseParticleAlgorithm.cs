﻿namespace POCOs.ParticleAlgorithms
{
    public abstract class BaseParticleAlgorithm
    {
        public abstract void Execute(int x, int y, Particle[,] particles);
        
        protected void SwapParticles(Vector2 a, Vector2 b, Particle[,] particles)
        {
            var firstParticle = particles[a.x, a.y];
            particles[a.x, a.y] = particles[b.x, b.y];
            particles[b.x, b.y] = firstParticle;
        }

        /// <summary>
        /// A custom, lightweight, non-Unity-dependant implementation of <code>Vector2Int</code>.
        /// Note: it's also possible to use <code>System.Drawing.Point</code> or a simple x-y tuple instead. 
        /// </summary>
        /// <param name="x">The X coordinate.</param>
        /// <param name="y">The Y coordinate.</param>
        protected struct Vector2
        {
            public int x, y;

            public Vector2(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }
    }
}