﻿namespace POCOs.ParticleAlgorithms
{
    public class SandAlgorithm : BaseParticleAlgorithm
    {
        public override void Execute(int x, int y, Particle[,] particles)
        {
            // If the particle sits at the bottom, leave it be.
            if (y < 1)
                return;

            var currentPosition = new Vector2(x, y);

            // First, see if the particle can fall directly below.
            var positionBelow = new Vector2(x, y - 1);
            var particleBelow = particles[positionBelow.x, positionBelow.y];

            if (particleBelow.particleType == ParticleType.Empty)
            {
                SwapParticles(currentPosition, positionBelow, particles);
                return;
            }

            // Next, see if it can fall below and to the left — but only if it's not a leftmost particle.
            if (x > 0)
            {
                var positionBelowLeft = new Vector2(x - 1, y - 1);
                var particleBelowLeft = particles[positionBelowLeft.x, positionBelowLeft.y];

                if (particleBelowLeft.particleType == ParticleType.Empty)
                {
                    SwapParticles(currentPosition, positionBelowLeft, particles);
                    return;
                }
            }

            // Finally, see if the particle can fall below and to the right — but only if it's not a rightmost particle.
            const int horizontalDimension = 0;
            var width = particles.GetLength(horizontalDimension);
            if (x < width - 1)
            {
                var positionBelowRight = new Vector2(x + 1, y - 1);
                var particleBelowRight = particles[positionBelowRight.x, positionBelowRight.y];
            
                if (particleBelowRight.particleType == ParticleType.Empty)
                    SwapParticles(currentPosition, positionBelowRight, particles);
            }
        }
    }
}