﻿using System;
using UnityEngine;

namespace Utilities
{
    public static class RenderScreenUtilities
    {
        public static int CalculateIndex(Vector2Int coordinates, int width) =>
            CalculateIndex(coordinates.x, coordinates.y, width);
        
        public static int CalculateIndex(int x, int y, int width)
        {
            if (x < 0) throw new ArgumentOutOfRangeException(nameof(x));
            if (y < 0) throw new ArgumentOutOfRangeException(nameof(y));
            if (width <= 0) throw new ArgumentOutOfRangeException(nameof(width));
            
            return y * width + x;
        }
        

        public static (int, int) CalculateCoordinates(int index, int width)
        {
            if (index < 0) throw new ArgumentOutOfRangeException(nameof(index));
            if (width <= 0) throw new ArgumentOutOfRangeException(nameof(width));
            
            int y = index / width;
            int x = index - y * width;
            return (x, y);
        }
    }
}
