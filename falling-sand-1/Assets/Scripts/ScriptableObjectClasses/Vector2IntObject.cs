﻿using UnityEngine;

namespace ScriptableObjectClasses
{
    [CreateAssetMenu(menuName = "Scriptable Objects/Vector2Int Object")]
    public class Vector2IntObject : ScriptableObject
    {
        public Vector2Int value;
    }
}
