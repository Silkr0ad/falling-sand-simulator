﻿using POCOs;
using UnityEngine;

namespace ScriptableObjectClasses
{
    [CreateAssetMenu(menuName = "Scriptable Objects/Simulator Particles Object")]
    public class SimulatorParticlesObject : ScriptableObject
    {
        [HideInInspector] public Particle[,] particles;
        [HideInInspector] public bool updateQueued;
    }
}
