﻿using System;
using System.Collections;
using POCOs;
using ScriptableObjectClasses;
using UnityEngine;
using UnityEngine.Serialization;

public class ParticleSimulator : MonoBehaviour
{
    public int Width => dimensions.value.x;
    public int Height => dimensions.value.y;

    public bool isPlaying = true;
    [SerializeField] private float stepsPerSecond = 10;
    [SerializeField] private Vector2IntObject dimensions;
    [SerializeField] private SimulatorParticlesObject particleOutput;

    private POCOs.ParticleSimulator _simulator;
    private Coroutine _simulationRoutine;

    private float StepTime => 1f / stepsPerSecond;

    private void Awake()
    {
        _simulator = new POCOs.ParticleSimulator(Width, Height);
    }

    private void Start()
    {
        _simulator.SetParticleIfFree(14, 7, new Particle(ParticleType.Sand));
        _simulator.SetParticleIfFree(14, 8, new Particle(ParticleType.Sand));
        _simulator.SetParticleIfFree(14, 9, new Particle(ParticleType.Sand));
        _simulator.SetParticleIfFree(14, 10, new Particle(ParticleType.Sand));

        StartSimulation();
    }

    public void StartSimulation() => _simulationRoutine = StartCoroutine(Simulate());
    public void StopSimulation() => StopCoroutine(_simulationRoutine);

    private IEnumerator Simulate()
    {
        while (isPlaying) {
            var startTime = DateTime.Now;

            _simulator.UpdateParticles();
            WriteToOutput();
            
            var endTime = DateTime.Now;
            var elapsedTime = (endTime - startTime).Seconds;

            if (elapsedTime >= StepTime)
                continue;
            
            yield return new WaitForSeconds(StepTime - elapsedTime);
        }
    }

    private void WriteToOutput()
    {
        particleOutput.particles = _simulator.Particles;
        particleOutput.updateQueued = true;
    }
}
