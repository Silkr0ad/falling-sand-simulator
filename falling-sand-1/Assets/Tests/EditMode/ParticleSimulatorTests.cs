﻿using NUnit.Framework;
using POCOs;
using UnityEngine;

namespace Tests.EditMode
{
    public class ParticleSimulatorTests
    {
        [Test]
        public void UpdateParticles_SandAtHeight1_FallsBelow()
        {
            // Arrange
            var simulator = CreateDefaultParticleSimulator(1, 2);
            simulator.Particles[0, 0] = new Particle(ParticleType.Empty);
            simulator.Particles[0, 1] = new Particle(ParticleType.Sand);

            // Act
            simulator.UpdateParticles();

            // Assert
            var particleAtTheBottom = simulator.Particles[0, 0];
            Assert.AreEqual(ParticleType.Sand, particleAtTheBottom.particleType);
        }

        [Test]
        public void UpdateParticles_SandAtHeight1_FallsBelowLeft()
        {
            // Arrange
            var simulator = CreateDefaultParticleSimulator(2, 2);
            simulator.Particles[0, 0] = new Particle(ParticleType.Empty);
            simulator.Particles[1, 0] = new Particle(ParticleType.Sand);
            simulator.Particles[1, 1] = new Particle(ParticleType.Sand);
            
            // Act
            simulator.UpdateParticles();
            
            // Assert
            var particleAtBottomLeft = simulator.Particles[0, 0];
            Assert.AreEqual(ParticleType.Sand, particleAtBottomLeft.particleType);
        }

        [Test]
        public void UpdateParticles_SandAtHeight1_FallsBelowRight()
        {
            // Arrange
            var simulator = CreateDefaultParticleSimulator(3, 2);
            simulator.Particles[0, 0] = new Particle(ParticleType.Sand);
            simulator.Particles[1, 0] = new Particle(ParticleType.Sand);
            simulator.Particles[1, 1] = new Particle(ParticleType.Sand);
            simulator.Particles[2, 0] = new Particle(ParticleType.Empty);
            
            // Act
            simulator.UpdateParticles();
            
            // Assert
            var particleAtBottomRight = simulator.Particles[2, 0];
            Assert.AreEqual(ParticleType.Sand, particleAtBottomRight.particleType);
        }
        
        private static POCOs.ParticleSimulator CreateDefaultParticleSimulator(int width, int height) =>
            new POCOs.ParticleSimulator(width, height);
    }
}
